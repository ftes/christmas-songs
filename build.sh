#!/bin/sh

BLANK=blank.pdf
ALL=""

for FILE in input/*; do
    PAGES=`pdftk "$FILE" dump_data | grep NumberOfPages | cut -d":" -f2`
    ALL="$ALL $FILE"
    [ $((PAGES%2)) -ne 0 ] && ALL="$ALL $BLANK"
done

pdftk $ALL cat output output.pdf
